
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>To Do List</title>
    <style>
        *{
    margin: 0;
    padding: 0;
    box-sizing: border-box;
}
body{
    background-color: #616091 ;
}
.container{
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #04041c ; 
    box-shadow:2px 5px 5px rgba(0,0,0,0.9);
    margin: 16px ;
    padding : 8px ;
    border : none;
    border-radius: 25px;

}
.top-heading{
    text-align: center;
    color: #fb97ad ;
    background-color: #04041c ; 
    box-shadow: 2px 5px 5px rgba(0,0,0,0.9);
    margin: 16px ;
    padding : 8px ;
    border : none;
    border-radius: 25px;

}
form{
    width: 100%;
}
.input-container{
    /* background-color: aqua; */
    width: 100%;
    display: flex;
    justify-content: center ;
    align-items : center;

}
#inputBox{
    width: 40%;
    height: 40px ;
    border: none;
    background-color: #04041c;
    border-bottom: 1px solid #616091;
    border-left: 1px solid #616091 ;
    color:white;
    text-align: center;
    font-size:16px ;
    transition:1s

}
#inp1{
    margin-left: 10px ;
    border:none;
    border-radius: 10px;
    background-color:#b4b3d8;
    font-size:large;
    padding:10px;
    width: 20%;
    transition:0.5s;
    color:#04041c;
}
#inp1:hover{
    cursor: pointer;
    background-color:#908fac;
    width: 22%;
}
#done{
    margin-left: 10px ;
    border:none;
    border-radius: 10px;
    background-color:#b4b3d8;
    font-size:15px;
    padding:5px;
    width: 10%;
    color:#04041c;
    float: right;
    margin-top:0px;
    height: 30px;
    
    

}
#done:hover{
    cursor: pointer;

}
#delete{
    margin-left: 10px ;
    border:none;
    border-radius: 10px;
    background-color:#fb97ad;
    font-size:15px;
    padding:5px;
    width: 10%;
    color:#04041c;
    float: right;
    height: 30px;
    
    
}
#delete:hover{
    cursor: pointer;

}
#ul1{
    display: block;
    justify-content: center;
    align-items: center;
    margin: 16px ;
    padding : 0px ;
    border : none;
    border-radius: 25px;
    list-style-type: none;


}
#li1{
    color:white;
    background-color: #04041c ; 
    box-shadow: 2px 5px 5px rgba(0,0,0,0.9);
    padding-left:20%;
    border-radius:25px;
    margin-bottom:20px;
    margin-right:14%;
    margin-left:15%;
    height:50px;
    padding-top:8px;

}

    </style>
   
</head>
<body>
<h1 class="top-heading">ToDo List</h1>
    <div class="container">
        <form action="" method="post">
            <div class="input-container">
            <input type="text" name="inputBox" id="inputBox" placeholder="Task Title" required>
            <button type="submit" value="action" name="add" id="inp1">Add</button>
            </div> 
        </form>
    </div>
    <?php
        include("listToDo.php")
        ?>

</body>
</html>
