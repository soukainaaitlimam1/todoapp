<?php
require("connection.php");
?>
<?php
if ($_SERVER["REQUEST_METHOD"]=="POST") {
   if (isset($_POST["add"])) {
    if (isset($_POST["inputBox"])) {
        $title = $_POST["inputBox"];
        $sql = "INSERT INTO todo (title) VALUES ('$title')";
        $conn->query($sql);
        header("Location: ".$_SERVER['PHP_SELF']); // Redirige après l'ajout
            exit();
    }
   }
   if (isset($_POST["done"])) {
    if (isset($_POST["id"])) {
        $id = $_POST["id"];
        $sql = "UPDATE todo SET done = 1 - done WHERE id=$id";
        $conn->query($sql);
        
    }
   }
   if (isset($_POST["delete"])) {
    if (isset($_POST["id"])) {
        $id = $_POST["id"];
        $sql = "DELETE FROM todo WHERE id=$id";
        $conn->query($sql);
    }
   }
}
?>
<?php 
$sql = "SELECT * FROM todo ORDER BY created_at DESC";
$result = $conn->query($sql);
$taches = array();
if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
        $taches[] = $row;
    }
}?>
<form action="" method="post">
<ul id="ul1">
<?php foreach ($taches as $tache): ?>
    <li id="li1"<?php echo ($tache['done'] == 1) ?>>
    <?php echo $tache["title"]; ?>
    <form action="" method="post">
    <input type="hidden" name="id" value="<?php echo $tache["id"]; ?>">
    <button type="submit" name="done" id="done">
    <?php echo ($tache['done'] == 1) ? 'Undo' : 'Done'; ?>
    </button>
    <button type="submit" name="delete" id="delete" >Delete</button>
    </form>
    </li>
    <?php endforeach; ?>
</ul>
</form>
